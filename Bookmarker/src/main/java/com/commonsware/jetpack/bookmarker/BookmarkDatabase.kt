/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val DB_NAME = "bookmarks.db"

@Database(entities = [BookmarkEntity::class], version = 1)
abstract class BookmarkDatabase : RoomDatabase() {

  abstract fun bookmarkStore(): BookmarkStore

  companion object {
    @Volatile
    private var INSTANCE: BookmarkDatabase? = null

    @Synchronized
    operator fun get(context: Context): BookmarkDatabase {
      if (INSTANCE == null) {
        INSTANCE =
          Room.databaseBuilder(context, BookmarkDatabase::class.java, DB_NAME)
            .build()
      }

      return INSTANCE!!
    }
  }
}
