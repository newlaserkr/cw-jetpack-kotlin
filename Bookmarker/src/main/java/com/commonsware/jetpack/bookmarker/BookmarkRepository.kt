/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import java.net.URI

object BookmarkRepository {
  suspend fun save(context: Context, pageUrl: String) =
    withContext(Dispatchers.IO) {
      val db: BookmarkDatabase = BookmarkDatabase[context]
      val entity = BookmarkEntity()
      val doc = Jsoup.connect(pageUrl).get()

      entity.pageUrl = pageUrl
      entity.title = doc.title()

      // based on https://www.mkyong.com/java/jsoup-get-favicon-from-html-page/

      val iconUrl: String? =
        doc.head().select("link[href~=.*\\.(ico|png)]").first()?.attr("href")
          ?: doc.head().select("meta[itemprop=image]").first().attr("content")

      if (iconUrl != null) {
        val uri = URI(pageUrl)

        entity.iconUrl = uri.resolve(iconUrl).toString()
      }

      db.bookmarkStore().save(entity)

      BookmarkModel(entity)
    }

  fun load(context: Context): Flow<List<BookmarkModel>> {
    val db: BookmarkDatabase = BookmarkDatabase[context]

    return db.bookmarkStore().all().map { entities ->
      entities.map { BookmarkModel(it) }
    }
  }
}
