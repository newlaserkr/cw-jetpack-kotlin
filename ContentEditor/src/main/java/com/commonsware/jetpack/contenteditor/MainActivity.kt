/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.observe
import com.commonsware.jetpack.contenteditor.databinding.ActivityMainBinding
import java.io.File

private const val FILENAME = "test.txt"
private const val REQUEST_SAF = 1337
private const val REQUEST_PERMS = 123

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModels()
  private var current: Uri? = null
  private lateinit var binding: ActivityMainBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    motor.results.observe(this) { result ->
      when (result) {
        StreamResult.Loading -> {
          binding.progress.visibility = View.VISIBLE
          binding.text.isEnabled = false
        }
        is StreamResult.Content -> {
          binding.progress.visibility = View.GONE
          binding.text.isEnabled = true
          current = result.source
          binding.title.text = result.source.toString()

          if (TextUtils.isEmpty(binding.text.text)) {
            binding.text.setText(result.text)
          }
        }
        is StreamResult.Error -> {
          binding.progress.visibility = View.GONE
          binding.text.setText(result.throwable.localizedMessage)
          binding.text.isEnabled = false
          Log.e("ContentEditor", "Exception in I/O", result.throwable)
        }
      }
    }

    loadFromDir(filesDir)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.loadInternal -> {
        loadFromDir(filesDir)
        return true
      }

      R.id.loadExternal -> {
        loadFromDir(getExternalFilesDir(null))
        return true
      }

      R.id.loadExternalRoot -> {
        loadFromExternalRoot()
        return true
      }

      R.id.openDoc -> {
        try {
          startActivityForResult(
            Intent(Intent.ACTION_OPEN_DOCUMENT)
              .setType("text/*")
              .addCategory(Intent.CATEGORY_OPENABLE), REQUEST_SAF
          )
        } catch (ex: ActivityNotFoundException) {
          Toast.makeText(
            this,
            "Sorry, we cannot open a document!",
            Toast.LENGTH_LONG
          ).show()
        }
        return true
      }

      R.id.newDoc -> {
        try {
          startActivityForResult(
            Intent(Intent.ACTION_CREATE_DOCUMENT)
              .setType("text/plain")
              .addCategory(Intent.CATEGORY_OPENABLE), REQUEST_SAF
          )
        } catch (ex: ActivityNotFoundException) {
          Toast.makeText(
            this,
            "Sorry, we cannot open a document!",
            Toast.LENGTH_LONG
          ).show()
        }
        return true
      }

      R.id.save -> {
        current?.let { motor.write(it, binding.text.text.toString()) }
        return true
      }
    }

    return super.onOptionsItemSelected(item)
  }

  override fun onActivityResult(
    requestCode: Int, resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_SAF) {
      if (resultCode == Activity.RESULT_OK && data != null) {
        binding.text.setText("")
        data.data?.let { motor.read(it) }
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<String>,
    grantResults: IntArray
  ) {
    if (requestCode == REQUEST_PERMS) {
      if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        loadFromDir(Environment.getExternalStorageDirectory())
      } else {
        Toast.makeText(this, R.string.msg_sorry, Toast.LENGTH_LONG).show()
      }
    }
  }

  private fun loadFromDir(dir: File?) {
    binding.text.setText("")
    motor.read(Uri.fromFile(File(dir, FILENAME)))
  }

  private fun loadFromExternalRoot() {
    if (ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
      ) == PackageManager.PERMISSION_GRANTED
    ) {
      loadFromDir(Environment.getExternalStorageDirectory())
    } else {
      val perms = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

      ActivityCompat.requestPermissions(this, perms, REQUEST_PERMS)
    }
  }
}
