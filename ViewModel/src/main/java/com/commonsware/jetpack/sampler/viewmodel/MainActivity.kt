/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "ViewModel"

class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val vm: ColorViewModel by viewModels()

    items.apply {
      layoutManager = LinearLayoutManager(this@MainActivity)
      addItemDecoration(
        DividerItemDecoration(
          this@MainActivity,
          DividerItemDecoration.VERTICAL
        )
      )
      adapter = ColorAdapter(layoutInflater).apply {
        submitList(vm.numbers)
      }
    }

    Log.d(TAG, "onCreate() called!")
  }

  override fun onStart() {
    super.onStart()

    Log.d(TAG, "onStart() called!")
  }

  override fun onResume() {
    super.onResume()

    Log.d(TAG, "onResume() called!")
  }

  override fun onPause() {
    Log.d(TAG, "onPause() called!")

    super.onPause()
  }

  override fun onStop() {
    Log.d(TAG, "onStop() called!")

    super.onStop()
  }

  override fun onDestroy() {
    Log.d(TAG, "onDestroy() called!")

    super.onDestroy()
  }
}
