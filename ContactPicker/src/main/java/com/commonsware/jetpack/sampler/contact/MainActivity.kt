/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.contact

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.commonsware.jetpack.sampler.contact.databinding.ActivityMainBinding

private const val REQUEST_PICK = 1337

class MainActivity : AppCompatActivity() {
  private val vm: ContactViewModel by viewModels()
  private lateinit var binding: ActivityMainBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding = ActivityMainBinding.inflate(layoutInflater)
    setContentView(binding.root)

    updateViewButton()

    binding.pick.setOnClickListener {
      try {
        startActivityForResult(
          Intent(
            Intent.ACTION_PICK,
            ContactsContract.Contacts.CONTENT_URI
          ), REQUEST_PICK
        )
      } catch (e: Exception) {
        Toast.makeText(this, R.string.msg_pick_error, Toast.LENGTH_LONG).show()
      }
    }

    binding.view.setOnClickListener {
      try {
        startActivity(Intent(Intent.ACTION_VIEW, vm.contact))
      } catch (e: Exception) {
        Toast.makeText(this, R.string.msg_view_error, Toast.LENGTH_LONG).show()
      }
    }
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_PICK) {
      if (resultCode == Activity.RESULT_OK &&
        data != null
      ) {
        vm.contact = data.data
        updateViewButton()
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }

  private fun updateViewButton() {
    if (vm.contact != null) {
      binding.view.isEnabled = true
    }
  }
}
