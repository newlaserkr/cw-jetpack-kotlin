/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.weather

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.*
import com.commonsware.jetpack.weather.databinding.RowBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val motor: MainMotor by viewModels()
    val adapter = WeatherAdapter()

    forecasts.layoutManager = LinearLayoutManager(this)
    forecasts.addItemDecoration(
      DividerItemDecoration(
        this,
        DividerItemDecoration.VERTICAL
      )
    )
    forecasts.adapter = adapter

    motor.results.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> progress.visibility = View.VISIBLE
        is MainViewState.Content -> {
          progress.visibility = View.GONE
          adapter.submitList(state.forecasts)
        }
        is MainViewState.Error -> {
          progress.visibility = View.GONE
          Toast.makeText(
            this@MainActivity, state.throwable.localizedMessage,
            Toast.LENGTH_LONG
          ).show()
          Log.e("Weather", "Exception loading data", state.throwable)
        }
      }
    }

    motor.load("OKX", 32, 34)
  }

  inner class WeatherAdapter :
    ListAdapter<RowState, RowHolder>(RowStateDiffer) {

    override fun onCreateViewHolder(
      parent: ViewGroup,
      viewType: Int
    ): RowHolder {
      return RowHolder(
        RowBinding.inflate(layoutInflater, parent, false)
      )
    }

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
      holder.bind(getItem(position))
    }
  }

  class RowHolder(private val binding: RowBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(state: RowState) {
      binding.state = state
      binding.executePendingBindings()
    }
  }

  object RowStateDiffer : DiffUtil.ItemCallback<RowState>() {
    override fun areItemsTheSame(
      oldItem: RowState,
      newItem: RowState
    ): Boolean {
      return oldItem === newItem
    }

    override fun areContentsTheSame(
      oldItem: RowState,
      newItem: RowState
    ): Boolean {
      return oldItem == newItem
    }
  }
}
