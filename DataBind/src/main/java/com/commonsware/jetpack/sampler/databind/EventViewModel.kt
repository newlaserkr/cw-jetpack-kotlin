/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.databind

import android.os.SystemClock
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import java.util.*

private const val STATE_EVENTS = "events"
private const val STATE_START_TIME = "startTime"

class EventViewModel(private val state: SavedStateHandle) : ViewModel() {
  val events: ArrayList<Event> = state.get(STATE_EVENTS) ?: arrayListOf()
  val startTime: Long = state.get(STATE_START_TIME)
    ?: SystemClock.elapsedRealtime().also { state.set(STATE_START_TIME, it) }
  private val id = Random().nextInt()

  fun addEvent(message: String, activityHash: Int) {
    events.add(Event(message, activityHash, id))
    state.set(STATE_EVENTS, events)
  }

  override fun onCleared() {
    events.clear()
  }
}
