/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import com.commonsware.jetpack.diceware.databinding.ActivityMainBinding

private const val REQUEST_OPEN = 1337

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModels()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    motor.results.observe(this) { viewState ->
      when (viewState) {
        MainViewState.Loading -> {
          binding.progress.visibility = View.VISIBLE
          binding.passphrase.text = ""
        }
        is MainViewState.Content -> {
          binding.progress.visibility = View.GONE
          binding.passphrase.text = viewState.passphrase
        }
        is MainViewState.Error -> {
          binding.progress.visibility = View.GONE
          binding.passphrase.text = viewState.throwable.localizedMessage
          Log.e(
            "Diceware",
            "Exception generating passphrase",
            viewState.throwable
          )
        }
      }
    }
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.open -> {
        val i = Intent()
          .setType("text/plain")
          .setAction(Intent.ACTION_OPEN_DOCUMENT)
          .addCategory(Intent.CATEGORY_OPENABLE)

        try {
          startActivityForResult(i, REQUEST_OPEN)
        } catch (ex: ActivityNotFoundException) {
          Toast.makeText(
            this,
            "Sorry, we cannot open a document!",
            Toast.LENGTH_LONG
          ).show()
        }
        return true
      }

      R.id.refresh -> {
        motor.generatePassphrase()
        return true
      }

      R.id.word_count_4, R.id.word_count_5, R.id.word_count_6, R.id.word_count_7,
      R.id.word_count_8, R.id.word_count_9, R.id.word_count_10 -> {
        item.isChecked = !item.isChecked
        motor.generatePassphrase(Integer.parseInt(item.title.toString()))

        return true
      }
    }

    return super.onOptionsItemSelected(item)
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_OPEN) {
      if (resultCode == Activity.RESULT_OK && data != null) {
        data.data?.let { motor.generatePassphrase(it) }
      }
    } else super.onActivityResult(requestCode, resultCode, data)
  }
}
