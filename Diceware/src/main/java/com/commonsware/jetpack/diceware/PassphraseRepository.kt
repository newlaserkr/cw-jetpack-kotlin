/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware

import android.content.Context
import android.net.Uri
import android.util.LruCache
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.security.SecureRandom
import java.util.*

val ASSET_URI: Uri =
  Uri.parse("file:///android_asset/eff_short_wordlist_2_0.txt")
private const val ASSET_FILENAME = "eff_short_wordlist_2_0.txt"

object PassphraseRepository {
  private val wordsCache = LruCache<Uri, List<String>>(4)
  private val random = SecureRandom()

  suspend fun generate(
    context: Context,
    wordsDoc: Uri,
    count: Int
  ): List<String> {
    var words: List<String>?

    synchronized(wordsCache) {
      words = wordsCache.get(wordsDoc)
    }

    return words?.let { rollDemBones(it, count, random) }
      ?: loadAndGenerate(context, wordsDoc, count)
  }

  private suspend fun loadAndGenerate(
    context: Context,
    wordsDoc: Uri,
    count: Int
  ): List<String> = withContext(Dispatchers.IO) {
    val inputStream: InputStream? = if (wordsDoc == ASSET_URI) {
      context.assets.open(ASSET_FILENAME)
    } else {
      context.contentResolver.openInputStream(wordsDoc)
    }

    inputStream?.use {
      val words = it.readLines()
        .map { line -> line.split("\t") }
        .filter { pieces -> pieces.size == 2 }
        .map { pieces -> pieces[1] }

      synchronized(wordsCache) {
        wordsCache.put(wordsDoc, words)
      }

      rollDemBones(words, count, random)
    } ?: throw IllegalStateException("could not open $wordsDoc")
  }

  private fun rollDemBones(
    words: List<String>,
    wordCount: Int,
    random: SecureRandom
  ) = List(wordCount) {
    words[random.nextInt(words.size)]
  }

  private fun InputStream.readLines(): List<String> {
    val result = ArrayList<String>()

    BufferedReader(InputStreamReader(this)).forEachLine { result.add(it); }

    return result
  }
}
